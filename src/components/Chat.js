import React from "react";
import { useLandbot } from "./hooks"
import {
  HeaderTitle,
  MessagesContainer,
  Message,
  Input
} from "./ui";

export default function Chat() {
  const [ messages, sendMessage ] = useLandbot("Type something to start a conversation with landbot.")
  return (
    <>
      <HeaderTitle size="subtitle">Landbot</HeaderTitle>
      <MessagesContainer>
        {messages.map(message => <Message key={message.key} {...message} />)}
      </MessagesContainer>
      <Input onSubmit={sendMessage} placeholder="Type here..." showButton />
    </>
  );
}
