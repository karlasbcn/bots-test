import React from "react"
import MessagesContainer from "./MessagesContainer"
import Message from "./Message"

export default {
  title: "Message",
  component: Message,
  decorator: (Story) => (
    <MessagesContainer>
      <Story />
    </MessagesContainer>
  )
}

const Template = (args) => <Message {...args} />

export const UserMessage = Template.bind({})

UserMessage.args = {
  author: "user",
  text: "Hello there"
}

export const BotMessage = Template.bind({})

BotMessage.args = {
  ...UserMessage.args,
  author: "bot"
}
