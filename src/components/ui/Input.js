import React, { useState } from 'react'
import PropTypes from 'prop-types'

function Input({ onSubmit, showButton, placeholder }) {
  const [ inputValue, setInputValue ] = useState('')

  function submit(){
    if (inputValue.length) {
      onSubmit(inputValue)
      setInputValue('')
    }
  }

  return (
    <div className="landbot-input-container">
      <div className="field">
        <div className="control">
          <input
            className="landbot-input"
            type="text"
            placeholder={placeholder}
            value={inputValue}
            onChange={e => setInputValue(e.target.value)}
            onKeyUp={e => {
              if (e.key === 'Enter') {
                e.preventDefault();
                submit();
              }
            }}
          />
          { showButton ? (<button
              className="button landbot-input-send"
              onClick={submit}
              disabled={inputValue === ''}
            >
              <span className="icon is-large">
                <i className="fas fa-paper-plane fa-lg"></i>
              </span>
            </button>)
            : null }
        </div>
      </div>
    </div>
  )
}

Input.propTypes = {
  /** Submit method */
  onSubmit: PropTypes.func.isRequired,
  /** Show submit button */
  showButton: PropTypes.bool,
  /** Input placeholder */
  placeholder: PropTypes.string
}

Input.defaultProps = {
  showButton: false,
  placeholder: ""
}

export default Input
