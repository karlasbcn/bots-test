import React from "react"
import HeaderTitle from "./HeaderTitle"

export default {
  title: "HeaderTitle",
  component: HeaderTitle,
}

const Template = (args) => <HeaderTitle {...args} />

export const Default = Template.bind({})

Default.args = {
  children: "Header",
}

export const Subtitle = Template.bind({})

Subtitle.args = {
  ...Default.args,
  size: "subtitle",
}
