import React from "react"
import { action } from "@storybook/addon-actions";
import Input from "./Input"

export default {
  title: "Input",
  component: Input
}

const Template = (args) => <Input {...args} />

export const Default = Template.bind({})

Default.args = {
  onSubmit: action("Submit")
}

export const WithButton = Template.bind({})

WithButton.args = {
  ...Default.args,
  showButton: true
}

export const WithPlaceholder = Template.bind({})

WithPlaceholder.args = {
  ...Default.args,
  placeholder: "Type here, please"
}
