import React from "react";

export const decorators = [
  (Story) => (
    <section id="landbot-app" className="hero is-fullheight is-dark">
      <Story />
    </section>
  ),
]
