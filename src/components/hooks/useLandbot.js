import { useState, useEffect, useRef, useMemo, useCallback } from "react"

/**
 * useLandbot: Landbot util hook.
 *
 * @param {String} initialMessage Bot welcome message.
 * @returns {Array} List of messages and send message method.
 */
function useLandbot(initialMessage){
  const [messages, setMessages] = useState([]);
  const coreRef = useRef()

  useEffect(() => {
    const core = new window.Landbot.Core({
      firebase: window.firebase,
      brandID: 12235,
      channelToken: 'H-441480-B0Q96FP58V53BJ2J',
      welcomeUrl: 'https://welcome.landbot.io/',
      welcome: [
        { samurai: -1, type: 'text', message: initialMessage }
      ],
    });

    core.pipelines.$readableSequence.subscribe(data => {
      setMessages(messages => ({
        ...messages,
        [data.key]: parseMessage(data),
      }))
    });

    core
      .init()
      .then(data => {
        setMessages(
          parseMessages(data.messages)
        );
      });

    coreRef.current = core

  }, [initialMessage])

  const _messages = useMemo(
    () => Object.values(messages)
      .filter(messagesFilter)
      .sort((a, b) => a.timestamp - b.timestamp)
    , [messages])

  const sendMessage = useCallback((message) => {
    coreRef.current.sendMessage({ message })
  }, [])

  return [_messages, sendMessage]
}

function parseMessages(messages) {
  return Object
    .values(messages)
    .reduce((obj, next) => {
      obj[next.key] = parseMessage(next);
      return obj;
    }, {});
}

function parseMessage(data) {
  return {
    key: data.key,
    text: data.title || data.message,
    author: data.samurai !== undefined ? 'bot' : 'user',
    timestamp: data.timestamp,
    type: data.type,
  };
}

function messagesFilter(data) {
  /** Support for basic message types */
  return ['text', 'dialog'].includes(data.type);
}

export default useLandbot
