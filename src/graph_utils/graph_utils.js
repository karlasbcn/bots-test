/**
 * GRAPH FUNCTIONS.
 */

/**
 * getConnections: Get all graph connections.
 *
 * @param {Object} graph Bot model object.
 * @returns {Array} Array of Connection models.
 */
export function getConnections(graph) {
  return Object.values(graph.connections)
}

/**
 * getNodes: Get all graph nodes.
 *
 * @param {Object} graph Bot model object.
 * @returns {Array} Array of Node models.
 */
export function getNodes(graph) {
  return Object.values(graph.nodes)
}

/**
 * getNodeInputConnections: Get a node input connections.
 *
 * @param {Object} node Node model object.
 * @param {Object} graph Bot model object.
 * @returns {Array} Array of Connection models.
 */
export function getNodeInputConnections(node, graph) {
  const { id } = node
  return getConnections(graph).filter(({ targetPath }) => id === targetPath)
}

/**
 * getNodeOutputConnections: Get a node output connections.
 *
 * @param {Object} node Node model object.
 * @param {Object} graph Bot model object.
 * @returns {Array} Array of Connection models.
 */
export function getNodeOutputConnections(node, graph) {
  const { id } = node
  return getConnections(graph).filter(({ sourcePath }) => id === sourcePath)
}

/**
 * getNodeConnections: Get a node connections.
 *
 * @param {Object} node Node model object.
 * @param {Object} graph Bot model object.
 * @returns {Array} Array of Connection models.
 */
export function getNodeConnections(node, graph) {
  const { id } = node
  return getConnections(graph).filter(
    ({ targetPath, sourcePath }) => [targetPath, sourcePath].includes(id)
  )
}

/**
 * getLeafNodes: Get all leaf nodes (no output connections).
 *
 * @param {Object} graph Bot model object.
 * @returns {Array} Array of Node models.
 */
export function getLeafNodes(graph) {
  return getNodes(graph).filter(node => getNodeOutputConnections(node, graph).length === 0)
}

/**
 * getRootNodes: Get all root nodes (no input connections).
 *
 * @param {Object} graph Bot model object.
 * @returns {Array} Array of Node models.
 */
export function getRootNodes(graph) {
  return getNodes(graph).filter(node => getNodeInputConnections(node, graph).length === 0)
}

/**
 * hasMultipleSources: Determines if a node is reachable from different sources.
 *
 * @param {Object} node Node model object.
 * @param {Object} graph Bot model object.
 * @returns {Boolean} True if a node has different source paths.
 */
export function hasMultipleSources(node, graph) {
  return getNodeInputConnections(node, graph).length > 1
}
