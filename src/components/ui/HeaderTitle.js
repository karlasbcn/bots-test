import React from 'react'
import PropTypes from 'prop-types'

function HeaderTitle({ children, size }) {
  return (
    <div className="landbot-header">
      <h1 className={size}>{ children }</h1>
    </div>
  )
}

HeaderTitle.propTypes = {
  /** Title text */
  children: PropTypes.string.isRequired,
  /** Font Size */
  size: PropTypes.oneOf(["title", "subtitle"]),
}

HeaderTitle.defaultProps = {
  size: "title",
}

export default HeaderTitle
