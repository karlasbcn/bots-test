import React, { useRef, useEffect } from 'react'
import PropTypes from 'prop-types'

function MessagesContainer({ children }) {
  const ref = useRef()

  useEffect(() => {
    const container = ref.current
    if (container){
      container.scrollTo({
        top: container.scrollHeight,
        behavior: 'smooth',
      })
    }
  }, [children.length])

  return (
    <div ref={ref} id="landbot-messages-container" className="landbot-messages-container">
      { children }
    </div>
  )
}

MessagesContainer.propTypes = {
  /** Message components */
  children: PropTypes.arrayOf(PropTypes.element).isRequired
}

export default MessagesContainer
